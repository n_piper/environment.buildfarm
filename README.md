# Buildfarm

Goal: Create a combined multi-machine deployment to support the needs of a build & release set of servers.

## References

Built on extension of the projects environment.buildbox, environment.repository.
The project is a vagrant Multimachine configuration.

## Pre-Requisites

* Vagrant
* Packer

Build the packer images referred to in the above projects so the ubuntu14 vagrant boxes are available locally.

Needs vagrant plugin:
vagrant plugin install vagrant-hostmanager

## Installation

```
> vagrant up
```


## Test the build
```
vagrant status

Current machine states:

buildbox                  running (virtualbox)
repository                running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.

```

## Where to use

```
Artifactory : http://localhost:8889
Jenkins:    : http://localhost:8888
```

The machines register via DHCP but have an alias where they can see each other at:

* solveapuzzledev-build (Jenkins)
* solveapuzzledev-repo (Artifactory)

Try the following command out to test this:

```
> vagrant ssh buildbox
> ping solveapuzzledev-repo
```

# References

http://www.cyberciti.biz/cloud-computing/use-vagrant-to-create-small-virtual-lab-on-linux-osx/
https://dzone.com/articles/running-maven-release-plugin
https://www.cloudbees.com/blog/orchestrating-deployments-jenkins-workflow-and-kubernetes
https://www.vagrantup.com/docs/multi-machine/
http://blogs.atlassian.com/2013/05/maven-git-flow-plugin-for-better-releases/
https://bitbucket.org/atlassian/jgit-flow/wiki/Home

# FEATURE -- Backup to Dropbox

http://www.jamescoyle.net/how-to/1147-setup-headless-dropbox-sync-client
https://www.jfrog.com/confluence/display/RTF/Advanced+Filestore+Configuration

The 'jewels' we want to back up are: (in order)

* The release repository of artifactory
* Jenkins Job configs & build numbers (ingore build results / packages for now)
* The snapshot repository of artifactory

```
# Need to install this
> sudo apt-get -y -q install python-gpgme
> wget https://www.dropbox.com/download?dl=packages/dropbox.py -O dropbox.py
> chmod +x dropbox.py
> printf 'y\n' | ./dropbox.py start -i

# FEATURE - Integrate to Maven

solveapuzzledev@gmail.com
Oauth Token: gC5osAbiuAAAAAAAAAAAChJFTWK5kPeNh9mun4bW2BZMBE_-Fd9ohXVo5KLXl_J1
APP: SOLVEAPUZZLEREPO
App Key: xcsrmdhnxbfmaf2
Secret: 163cpl5t000yct2

https://www.dropbox.com/developers/reference/oauth-guide

HTTP API - https://www.dropbox.com/developers-v1/core/docs
Java - http://dropbox.github.io/dropbox-sdk-java/api-docs/v1.8.x/
https://www.dropbox.com/developers-v1/reference/devguide
 	

# FEATURE - Do we need Artifactory for Maven builds?

A Maven component (a wagon) that enables deploying artifacts in remote Git SCM repositories
http://synergian.github.io/wagon-git/index.html


## Part 2 - Maven on Dropbox

Using the file wagon you can deploy your artifacts to a personal repo on DropBox

https://code.google.com/archive/p/peter-lavalle/wikis/MavenOnDropBox.wiki


