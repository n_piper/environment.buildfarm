$snapshotlocal = {
   id => "snapshot-local",
   url => "http://solvepuzzledev-repo:8080/artifactory/libs-snapshot-local",
   username => "admin",
   password => "password"
}

$releaselocal = {
   id => "release-local",
   url => "http://solvepuzzledev-repo:8080/artifactory/libs-release-local",
   username => "admin",
   password => "password"
}

$central = {
  id => "central",
  url => "http://repo.maven.apache.org/maven2"
}


 # Install Maven
 class { "maven::maven":
  version => "3.2.2", # version to install
  # you can get Maven tarball from a Maven repository instead than from Apache servers, optionally with a user/password
  repo => {
    #url => "http://repo.maven.apache.org/maven2",
    #username => "",
    #password => ""
  }
}
# Create a settings.xml with the repo credentials
#maven::settings { 'maven-user-settings' :  
#servers => [ $snapshotlocal,$releaselocal], # servers entry in settings.xml, uses id, username, password from the hash passed
 # user    => 'vagrant',
#default_repo_config => {
#
#    url       => $snapshotlocal['url'],
#    snapshots => {
#      enabled      => 'true',
#      updatePolicy => 'always'
#    },

 # }
#}