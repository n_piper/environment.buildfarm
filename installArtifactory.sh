echo "Australia/Melbourne" | sudo tee /etc/timezone
dpkg-reconfigure --frontend noninteractive tzdata

# Install Java 8 JDK
# http://tecadmin.net/install-oracle-java-8-jdk-8-ubuntu-via-ppa/
cat - <<-EOF >> /etc/apt/sources.list.d/webupd8team-java.list
# webupd8team repository list 
deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main
# deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main
EOF

apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xEEA14886

echo debconf shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | /usr/bin/debconf-set-selections

apt-get update
apt-get install -y -q oracle-java8-installer


# Install Artifactory
# https://www.jfrog.com/confluence/display/RTF/Installing+on+Linux+Solaris+or+Mac+OS#InstallingonLinuxSolarisorMacOS-RPMorDebianInstallation

wget https://bintray.com/artifact/download/jfrog/artifactory/jfrog-artifactory-oss-4.6.0.zip
unzip jfrog-artifactory-oss-4.6.0.zip -d /opt

# Overwrite the artifactory.default with JDK8 java home and opts
cp /vagrant_data/artifactory.default /opt/artifactory-oss-4.6.0/bin/artifactory.default

/opt/artifactory-oss-4.6.0/bin/installService.sh vagrant

# Check & start artifactory 
/etc/init.d/artifactory check
/etc/init.d/artifactory start
